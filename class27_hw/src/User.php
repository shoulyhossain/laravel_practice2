<?php
class User{

   public $conn;
   public function __construct()
   {    
       $db='ecommerceb4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
public function store($data)
    {
        try{
            $filename=$_FILES['image']['name'];
            $tempname=$_FILES['image']['tmp_name'];
            $uniqueImageName=time().$filename;
            move_uploaded_file($tempname,'../assets/images/'.$uniqueImageName);
           
            $fname = $data["fname"];
            $lname = $data["lname"];
            $mail =  $data["mail"];
            $phone = $data["phone"];
            $dob = $data['dob'];
            $gender =$data['gender'];
            $division =$data['division'];
          

            $query = "insert into user(fname,lname,mail,phone,dob,gender,division,image) values(:enter_fname, :enter_lname,:enter_mail, :enter_phone,:dob,:gender,:division,:image)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_fname' => $fname,
                    'enter_lname' => $lname,
                    'enter_mail' => $mail,
                    'enter_phone' => $phone,
                    'dob'=>$dob,
                    'gender'=>$gender,
                    'division'=>$division,
                    'image'=>$uniqueImageName
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message'] = $exception->getMessage();
            header('location: ../view/create.php');
        }
    }
    public function index()
    {
        $query = "select * from user";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function show($id)
    {
        $query = "select * from user where id = :user_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(
           [ 
               'user_id' => $id 
           ]
        );
        $data = $stmt->fetch();
        return $data;
    }
    public function update($data)
    {   
        try{
            $filename=$_FILES['image']['name'];
            $tempname=$_FILES['image']['tmp_name'];
             $uniqueImageName=time().$filename;
            
             move_uploaded_file($tempname,'../assets/images/'.$uniqueImageName);
            $id = $data['id'];
            $fname = $data["fname"];
            $lname = $data["lname"];
            $mail =  $data["mail"];
            $phone = $data["phone"];
            $dob = $data['dob'];
            $gender =$data['gender'];
            $division =$data['division'];

            
            $query = "update user set fname=:enter_fname,lname=:enter_lname,mail=:enter_mail,phone=:enter_phone,dob=:dob, gender=:gender, division =:division,image=:image where id =:id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [   
                    'id'=>$id,
                    'enter_fname' => $fname,
                    'enter_lname' => $lname,
                    'enter_mail' => $mail,
                    'enter_phone' => $phone,
                    'dob'=>$dob,
                    'gender'=>$gender,
                    'division'=>$division,
                    'image'=>$uniqueImageName
                    ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message'] = $exception->getMessage();
            // header('location: ../view/update.php');
        }
        
    }
    public function delete($id)
    {
        $query = "delete from user where id = :user_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(
           [ 'user_id' => $id]
        );
       $stmt->fetch();
       header('location: ../view/index.php');
    }

}