<?php
include_once ("../src/User.php");
$userObj = new User();
$values=$userObj->show($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="create.php">Add new</a>
    <a href="edit.php?id=<?=$values['id']?>"> edit</a>
<table border="1">
        <thead>
            <tr>
                <th>first name</th>
                <th>last name</th>
                <th>email</th>
                <th>phone no</th>
                <th>DOB</th>
                <th>gender</th>
                <th>division</th>
                <th>image</th>
            </tr>
        </thead>
        <tbody>
       
            <tr>
                <td> <?= $values['fname'] ?></td>
                <td><?=$values['lname']?></td>
                <td> <?= $values['mail'] ?></td>
                <td><?=$values['phone']?></td>
                <td> <?= $values['dob'] ?></td>
                <td><?=$values['gender']?></td>
                <td> <?= $values['division'] ?></td>
                <td><img style="width: 50px;" src="../assets/images/<?=$values['image']?>" alt="no image/badimage"></td>
            </tr> 
            

        </tbody>
    </table>
    <a  style="text-decoration: none; color:red;" href="delete.php?id=<?=$values['id']?>"> delete</a>
</body>
</html>

