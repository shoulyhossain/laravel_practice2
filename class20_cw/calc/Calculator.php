<?php
class Calculator
{
public $number1;
public $number2;

public function __construct($num1, $num2)
{
 $this->number1 = $num1;
 $this -> number2 = $num2;
}
public function sum()
{
 return $this-> number1 + $this -> number2;
}
public function sub()
{
 return $this-> number1 - $this -> number2;
}
public function div()
{
 return $this-> number1 / $this -> number2;
}
public function mul()
{
 return $this-> number1 * $this -> number2;
}
}
$calculatorObject = new  Calculator($_POST["num1"], $_POST["num2"]) ;
if(isset($_POST["sum"]))
{
    echo $calculatorObject-> sum();
}
if(isset( $_POST["sub"]))
{
    echo $calculatorObject-> sub();
}
if(isset( $_POST["div"]))
{
    echo $calculatorObject-> div();
}
if(isset($_POST["mul"]))
{
    echo $calculatorObject-> mul();
}
?>