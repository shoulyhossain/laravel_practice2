<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ColorsController;
use App\Http\Controllers\SizesController;
use App\Http\Controllers\BrandsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[FrontendController::class, 'index'])->name('main');






Route::prefix('dashboard')->group(function () {
    
Route::get('/', function () {
    return view('backend/index');
})->name('dashboard');

Route::get('/products',[ProductsController::class, 'index'])->name('dashboard.products');
Route::get('/products/create',[ProductsController::class, 'create'])->name('dashboard.products.create');
Route::post('/products/store',[ProductsController::class, 'store'])->name('dashboard.products.store');
Route::get('/products/{id}',[ProductsController::class, 'show'])->name('dashboard.products.show');
Route::get('/products/{id}/edit',[ProductsController::class, 'edit'])->name('dashboard.products.edit');
Route::patch('/products/{id}',[ProductsController::class, 'update'])->name('dashboard.products.update');
Route::delete('/products/{id}',[ProductsController::class, 'destroy'])->name('dashboard.products.destroy');


Route::get('/categories',[ CategoriesController::class, 'index'])->name('dashboard.categories');
Route::get('/categories/create',[ CategoriesController::class, 'create'])->name('dashboard.categories.create');
Route::post('/categories/store',[ CategoriesController::class, 'store'])->name('dashboard.categories.store');
Route::get('/categories/{id}',[CategoriesController::class, 'show'])->name('dashboard.categories.show');
Route::get('/categories/{id}/edit',[CategoriesController::class, 'edit'])->name('dashboard.categories.edit');
Route::patch('/categories/{id}',[CategoriesController::class, 'update'])->name('dashboard.categories.update');
Route::delete('/categories/{id}',[CategoriesController::class, 'destroy'])->name('dashboard.categories.destroy');


Route::get('/colors',[ ColorsController::class, 'index'])->name('dashboard.colors');
Route::get('/colors/create',[ ColorsController::class, 'create'])->name('dashboard.colors.create');
Route::post('/colors/store',[ ColorsController::class, 'store'])->name('dashboard.colors.store');
Route::get('/colors/{id}',[ColorsController::class, 'show'])->name('dashboard.colors.show');
Route::get('/colors/{id}/edit',[ColorsController::class, 'edit'])->name('dashboard.colors.edit');
Route::patch('/colors/{id}',[ColorsController::class, 'update'])->name('dashboard.colors.update');
Route::delete('/colors/{id}',[ColorsController::class, 'destroy'])->name('dashboard.colors.destroy');



});


Route::get('/dashboard/colors',[ ColorsController::class, 'index'])->name('dashboard.colors');
Route::get('/dashboard/colors/create',[ ColorsController::class, 'create'])->name('dashboard.colors.create');
Route::post('/dashboard/colors/store',[ ColorsController::class, 'store'])->name('dashboard.colors.store');

Route::get('/dashboard/sizes',[ SizesController::class, 'index'])->name('dashboard.sizes');
Route::get('/dashboard/sizes/create',[ SizesController::class, 'create'])->name('dashboard.sizes.create');
Route::post('/dashboard/sizes/store',[ SizesController::class, 'store'])->name('dashboard.sizes.store');

Route::get('/dashboard/brands',[ BrandsController::class, 'index'])->name('dashboard.brands');
Route::get('/dashboard/brands/create',[ BrandsController::class, 'create'])->name('dashboard.brands.create');
Route::post('/dashboard/brands/store',[ BrandsController::class, 'store'])->name('dashboard.brands.store');
