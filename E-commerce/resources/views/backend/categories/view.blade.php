<x-backend.layouts.master>
<h3>Category details</h3>
    <p>name: {{$category->title}}</p>

    <p>description: {{$category->description}}</p>

    <p>Created at: {{$category->created_at}}</p>
    <br>


    <br>

   <a class="btn btn-success" href="{{route('dashboard.categories')}}"> Go back to category list</a>
   <a href="{{route('dashboard.categories.edit', ['id'=>$category->id])}}" class="btn btn-primary">Edit</a>
</x-backend.layouts.master>