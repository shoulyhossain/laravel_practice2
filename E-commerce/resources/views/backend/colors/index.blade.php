<x-backend.layouts.master>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
           All colors
        </div>
        <div><a href="{{route('dashboard.colors.create')}}"><button class="btn btn-outline-success">Add new color</button></a></div>
        <div class="card-body">
            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Action</th>
                       
                  
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($colors as $color)
                        
                   
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{Str::limit($color->title, 20) }}</td>
                        <td>{{Str::limit($color->description, 50)}}</td>
                        <td>
                        <a href="{{ route('dashboard.colors.show', ['id' => $color->id]) }}"><button
                                        class="btn btn-primary">view</button></a>
                                <a href="{{ route('dashboard.colors.edit', ['id' => $color->id]) }}"><button
                                        class="btn btn-warning">edit</button></a>
                                <form style="display: inline" method="POST"
                                    action="{{ route('dashboard.colors.destroy', ['id' => $color->id]) }} ">
                                    @method('delete')
                                    @csrf
                                     <button type="submit" onclick="return confirm('This operation will delete the item!')" class="btn btn-danger">Delete</button>
                                </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>