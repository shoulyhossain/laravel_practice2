<x-backend.layouts.master>
    <div class="container">
        <h2>Edit color</h2>
        <a href="{{route('dashboard.colors')}}"><button class="btn btn-outline-success">color list</button></a>


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="
        {{route('dashboard.colors.update',['id'=>$color->id])}}
        " method="POST" class="py-3">
            @csrf
            @method('patch')
            <div>
                <label for="exampleInputEmail1" class="form-label">Title</label>
                <input type="text" class="form-control" id="exampleInputEmail1" name="title" value="{{old('title',$color->title)}}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description',$color->description)}}</textarea>
            </div>
            
            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</x-backend.layouts.master>
