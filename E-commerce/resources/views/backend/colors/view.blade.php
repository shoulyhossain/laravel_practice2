<x-backend.layouts.master>
<h3>Color details</h3>
    <p>name: {{$color->title}}</p>

    <p>description: {{$color->description}}</p>

    <p>Created at: {{$color->created_at}}</p>
    <br>


    <br>

   <a class="btn btn-success" href="{{route('dashboard.colors')}}"> Go back to color list</a>
   <a href="{{route('dashboard.colors.edit', ['id'=>$color->id])}}" class="btn btn-primary">Edit</a>
</x-backend.layouts.master>