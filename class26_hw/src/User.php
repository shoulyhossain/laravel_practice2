<?php
class User{

   public $conn;
   public function __construct()
   {    
       $db='ecommerceb4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
public function store($data)
    {
        try{
           
            $fname = $data["fname"];
            $lname = $data["lname"];
            $mail =  $data["mail"];
            $phone = $data["phone"];
            $query = "insert into user(fname,lname,mail,phone) values(:enter_fname, :enter_lname,:enter_mail, :enter_phone)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_fname' => $fname,
                    'enter_lname' => $lname,
                    'enter_mail' => $mail,
                    'enter_phone' => $phone
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message'] = $exception->getMessage();
            header('location: ../view/create.php');
        }
    }
    public function index()
    {
        $query = "select * from user";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function show($id)
    {
        $query = "select * from user where id = :user_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(
           [ 
               'user_id' => $id 
           ]
        );
        $data = $stmt->fetch();
        return $data;
    }
    public function update($data)
    {   print_r($data);
        try{
            $id = $data['id'];
            $fname = $data["fname"];
            $lname = $data["lname"];
            $mail =  $data["mail"];
            $phone = $data["phone"];
            
            $query = "update user set fname=:enter_fname,lname=:enter_lname,mail=:enter_mail,phone=:enter_phone where id=:id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [   
                    'id'=>$id,
                    'enter_fname' => $fname,
                    'enter_lname' => $lname,
                    'enter_mail' => $mail,
                    'enter_phone' => $phone
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message'] = $exception->getMessage();
            header('location: ../view/update.php');
        }
        
    }
    public function delete($id)
    {
        $query = "delete from user where id = :user_id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(
           [ 'user_id' => $id]
        );
       $stmt->fetch();
       header('location: ../view/index.php');
    }

}