<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
include_once ("../src/User.php");
$userObj = new User();
$user = $userObj->index();

?>
<?php
    if(isset($_SESSION['message']))
    {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
    ?>
    <h2>Users</h2>
    <a href="create.php">Add new</a>
    <table border="1">
        <thead>
            <tr>
                <th>Id</th>
                <th>first name</th>
                <th>last name</th>
                <th>email</th>
                <th>phone no</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $SL=1;
            foreach($user as $values){
            ?>
            <tr>
                <td><?=$SL++ ?></td>
                <td> <?= $values['fname'] ?></td>
                <td><?=$values['lname']?></td>
                <td> <?= $values['mail'] ?></td>
                <td><?=$values['phone']?></td>
                <td><a href="show.php?id=<?=$values['id']?>">show</a>
                <a href="edit.php?id=<?=$values['id']?>">edit</a> 
                <a href="delete.php?id=<?=$values['id']?>">delete</a>
            </td>
            </tr>  <?php } ?>
            
        </tbody>
    </table>
</body>
</html>