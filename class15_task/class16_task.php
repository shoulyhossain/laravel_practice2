<?php
$students = [
    'cse' => [
        ['name' => 'Himel', 'email' => 'himel@gmail.com', 'result' => '4'],
        ['name' => 'Zarin', 'email' => 'zarin@gmail.com', 'result' => '4']
    ],
    'bba' => [
        ['name' => 'Tamim', 'email' => 'tamim@gmail.com', 'result' => '4'],
        ['name' => 'Kafi', 'email' => 'kafi@gmail.com', 'result' => '3.9']
    ]
];
foreach($students as $key=> $value){
    echo "Department: ".strtoupper($key)."<br>";
    echo "Students:<br>";
    foreach($value as $keys=> $values)
    {
        echo ($keys+1).". ";
        foreach( $values as $k=>$v)
        {
            echo ucwords($k).": ".$v.", ";
        }
        echo "<br>";
    }echo "<br>";
}
?>