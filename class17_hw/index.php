<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Super global variable</title>
</head>
<body>
<?php
if(isset($_SESSION["message"]))
{
    echo $_SESSION["message"];
    unset($_SESSION["message"]);
}
?>
    <form action="form.php" method="POST">
        <input type="text" name="name" placeholder="Enter your name"\>
        <input type="password" name="password" placeholder="Enter your password"\>
        <button type="button">Login</button>
    </form>
</body>
</html>