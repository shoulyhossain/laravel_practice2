<?php
// //no.1
// class Greeter
// {
//     public $message = "welcome"; 
//     //no.3
//     public function hello()
//     {
//         return $this -> message; 
//     }      
// }
// //no.2
// $greeterObject = new Greeter();
// echo $greeterObject -> message; 



// //no.4
// class Car 
// {
//     public $company = NULL;
//     public $color = "beige";
//     public function describe()
//     {
//         return "Beep! I am $this->company, my color is $this->color";
//     }
// }

// $bmw = new Car();
// $bmw -> company = "BMW";
// echo $bmw -> describe();



// //no.5
// class Calculator
// {
//     public $multply_result;
//     public $add_result;
//     public $subtract_result;
//     public $division_result;
//     public function multiply($num1, $num2)
//     {
//         $this->multply_result = $num1 * $num2;
//         // echo "Mutiplication is ".$this->multply_result."<br>";
//     }
//     public function add($num1, $num2)
//     {
//         $this->add_result = $num1 + $num2;
//         // echo "Addition is ".$this->add_result."<br>";
//     }
//     public function subtract($num1, $num2)
//     {
//         $this->subtract_result = $num1 - $num2;
//         // echo "Subtraction is ".$this->subtract_result."<br>";
//     }
//     public function division($num1, $num2)
//     {
//         $this->division_result = $num1 / $num2;
//         // echo "Division is ".$this->subtract_result."<br>";
//     }
// }
// $calculator = new Calculator();
// $calculator->multiply(22, 23);
// $calculator->add(22, 20);
// $calculator->subtract(50, 23);
// $calculator->division(20, 10);



// //no.6
// class Student
// {
//     public $firstname;
//     public $lastname;
//     public $age;
//     public function setProperties($fname, $lname, $age)
//     {
//         $this->firstname = $fname;
//         $this->lastname = $lname;
//         $this->age = $age;
//     }
// }
// $student1 = new Student();
// $student1->setProperties("Jefrey", "Way", 34);

// $student2 = new Student();
// $student2->setProperties("John", "Williamson", 20);

// $student3 = new Student();
// $student3->setProperties("Jyoti", "Khan", 24);

// $student4 = new Student();
// $student4->setProperties("Shouly", "Hossain", 22);

// $student5 = new Student();
// $student5->setProperties("Joy", "Islam", 15);

// $sum = $student1->age + $student2->age  + $student3->age + $student4->age + $student5->age;
// $average = $sum / 5;
// echo "Average of ages of all stuednts is: ".$average;




// //no.7
class Animal
{
    public $name;
    public $speak;
    public $is_tail;
    public $leg;
    public function __construct($n, $s, $tail, $l)
    {
        $this->name = $n;
        $this->speak = $s;
        $this->is_tail = $tail;
        $this->leg = $l;
    }
}
$cat = new Animal("cat", "meow", true, 4);
$human = new Animal("human", "human language", false, 2);
print($cat->name);
echo "  ";
print($cat->speak);
echo "  ";
print($cat->is_tail);
echo "  ";
print($cat->leg);


//n0.8
class Animal
{
   public $name;
   public $exist = "Earth";
   public $eyes = 2;
   public $legs;
   public $has_tail = false;
}
class Cat extends Animal
{
    public function setName($n)
    {
       $this->name=$n;
    }
    public function setLegs($l)
    {
        $this->legs=$l;
    }
    public function describe()
    {
        return "I am $this->name, I exist on $this->exist, I have $this->eyes & $this->legs legs.";
    }
}
$catObject = new Cat();
$catObject->setName("cat");
$catObject->setLegs(4);
echo $catObject->describe();
?>