<?php
include_once("Displayer.php");
class Square extends Shape
{
    use Displayer;
    public $length;
    public function __construct($l)
    {
        $this->length=$l;
    }
    public function area()
    {
        return $this->length * $this->length;
    }
}
?>