<?php
session_start();
class Customer
{
    public $conn;
    public function __construct()
    {
        try{
          $this->conn =  new PDO("mysql:host=localhost;dbname=ecommerce", "root", "root");
          $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception){
         $_SESSION['message']= $exception->getMessage();
         header('location: ../views/create.php');
        }
        
    }


    public function store($data)
    {
         try{
            $fname = $data['fname'];
            $lname =  $data['lname'];
            $address = $data['address'];
            $query = "insert into customer(fname, lname, address) values(:firstName, :lastName, :address )";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                ['firstName' => $fname,
                'lastName' => $lname,
                 'address' =>  $address
                ]
            );
            $_SESSION['message']= 'upload successfull';
            header('location: ../views/create.php');

            }catch(PDOException $exception){
                $_SESSION['message']= $exception->getMessage();
                header("location: ../views/create.php");
            }
        
       
    }
}