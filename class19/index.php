<?php
class Calculator
{
    public $number1, $number2;
    public function __construct($num1, $num2)
    {
        $this -> number1 = $num1;
        $this -> number2 = $num2;
    }
    public function sum()
    {
        return $this -> number1 + $this -> number2 ;
    }
    public function sub()
    {
        return $this -> number1 - $this -> number2 ;
    }    
    public function div()
    {
        return $this -> number1 / $this -> number2 ;
    }
    public function mul()
    {
        return $this -> number1 * $this -> number2 ;
    }

}

$calculatorObject = new Calculator(20, 10);
echo $calculatorObject -> sum()."<br>";
echo $calculatorObject -> sub()."<br>";
echo $calculatorObject -> div()."<br>";
echo $calculatorObject -> mul()."<br>";
?>