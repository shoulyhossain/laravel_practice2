<?php
class Categories{

   public $conn;
   public function __construct()
   {
       try{
        $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce_b4", "root", "root");
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage();
        header('location: ../view/create.php');
       }
   }

public function store($data)
{
    try{
        session_start();
        $title = $data["title"];
        $description = $data["description"];
        $query = "insert into categories(title, description) values(:enter_title, :enter_description)";
        $stmt = $this->conn->prepare($query);
        $stmt->execute(
            [
                'enter_title' => $title,
                'enter_description' => $description
            ]
        );
        $_SESSION['message'] = 'successful';
        header('location: ../view/create.php');
       }catch(PDOException $exception)
       {
           $_SESSION['message']->getMessage();
           header('location: ../view/create.php');
       }
}
}
?>