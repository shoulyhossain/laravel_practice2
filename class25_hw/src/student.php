<?php
class Student{

   public $conn;
   public function __construct()
   {    
       $db='ecommerce_b4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
   public function students($dt)
    {
        try{
           
            $name = $dt["name"];
            $id = $dt["id"];
            $query = "insert into student(name,id) values(:enter_name, :enter_id)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_name' => $name,
                    'enter_id' => $id
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message']->getMessage($exception);
            header('location: ../view/create.php');
        }
    }
    public function students_index()
    {
        $query = "select * from student";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}