<?php
class Categories{

   public $conn;
   public function __construct()
   {    
       $db='ecommerce_b4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
    
    public function index()
    {
        $query = "select * from categories";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }


    public function store($data)
    {
        try{
            
            $title = $data["title"];
            $description = $data["description"];
            $query = "insert into categories(title, description) values(:enter_title, :enter_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_title' => $title,
                    'enter_description' => $description
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message']->getMessage($exception);
            header('location: ../view/create.php');
            
        }
    }


}
