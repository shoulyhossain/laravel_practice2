<?php
class User{

   public $conn;
   public function __construct()
   {    
       $db='ecommerce_b4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
public function user($dta)
    {
        try{
           
            $fname = $dta["fname"];
            $lname = $dta["lname"];
            $mail =  $dta["mail"];
            $phone = $dta["phone"];
            $query = "insert into user(fname,lnmae,mail,phone) values(:enter_fname, :enter_lname,:enter_mail, :enter_phone)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_fname' => $fname,
                    'enter_lname' => $lname,
                    'enter_mail' => $mail,
                    'enter_phone' => $phone
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message'] = $exception->getMessage($exception);
            header('location: ../view/create.php');
        }
    }
    public function user_index()
    {
        $query = "select * from user";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}