<?php
class Product{

   public $conn;
   public function __construct()
   {    
       $db='ecommerce_b4';
       $u='root';
       $p='root';
       try{
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=$db", $u, $p);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       }catch(PDOException $exception)
       {
        $_SESSION['message']->getMessage($exception);
        header('location: ../view/create.php');
       }
   }
   public function products($d)
    {
        try{
           
            $name = $d["p_name"];
            $price = $d["p_price"];
            $query = "insert into products(name,price) values(:enter_name, :enter_price)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'enter_name' => $name,
                    'enter_price' => $price
                ]
            );
            $_SESSION['message'] = 'successful';
            header('location: ../view/index.php');
        }catch(PDOException $exception)
        {
            $_SESSION['message']->getMessage($exception);
            header('location: ../view/create.php');
        }
    }
    public function product_index()
    {
        $query = "select * from products";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }
}