<?php
include_once('../src/User.php');
$userObj = new User();
$values=$userObj->show($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php
       if(isset( $_SESSION['error'])){
           echo "<span>".$_SESSION['error']."</span>";
           unset($_SESSION['error']);
       }
    ?>
    <a href="create.php"><button>Add new </button></a> 
<a href="index.php"><button>View all </button></a><br>

<h3>Edit your data:</h3>
    <div>
        <form action="update.php" method="POST" >
        <input type="text" value="<?=$values['id']?>" name="id" hidden>
        <input type="text" name="name" value="<?=$values['name']?>" required><br><br>
        <input type="number" name="phone" value="<?=$values['phone']?>" required><br><br>
        <button>update</button>
        </form>
    </div>
</body>
</html>