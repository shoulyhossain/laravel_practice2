<?php
include_once("../src/User.php");
$userObj = new User;
$user = $userObj->retrive_data();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
</head>

<body>
    <?php
    if (isset($_SESSION['success'])) {
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
    ?>
    <a href="create.php"><button>Add new </button></a>
    <a href="trash.php"><button>trash</button></a><br>


    <table border="1">
        <thead>
            <th>#</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Action</th>
        </thead>
        <tbody>
            <?php 
                $sl=1;
                foreach($user as $values){

            
            ?>
            <tr>
                <td><?= $sl++ ?></td>
                <td><?= $values['name'] ?></td>
                <td><?= $values['phone'] ?></td>
                <td><a href="view.php?id=<?=$values['id']?>"><button>View</button></a><a href="edit.php?id=<?=$values['id']?>"><button>edit</button></a> <a href="destroy.php?id=<?=$values['id']?>"><button style="color: red;" onclick="return confirm('are you sure?')">Delete</button></a></td>
            </tr>
            <?php } ?>
        </tbody>


    </table>

</body>

</html>