<?php
include_once('../src/User.php');
$userObj = new User();
$trash=$userObj->trash();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<a href="index.php"><button>View all </button></a><br>
<?php
    if (isset($_SESSION['success'])) {
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
    if (isset($_SESSION['error'])) {
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
    ?>
<h3>your deleted items are here:</h3>
<table border="1">
        <thead>
            <th>#</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Action</th>
        </thead>
        <tbody>
            <?php 
                $sl=1;
                foreach($trash as $values){

            
            ?>
            <tr>
                <td><?= $sl++ ?></td>
                <td><?= $values['name'] ?></td>
                <td><?= $values['phone'] ?></td>
                <td><a href="restore.php?id=<?=$values['id']?>"><button>Restore</button></a> <a href="delet.php?id=<?=$values['id']?>"><button style="color: red;" onclick="return confirm('are you sure?')">Delete</button></a></td>
            </tr>
            <?php } ?>
        </tbody>


    </table>
</body>
</html>