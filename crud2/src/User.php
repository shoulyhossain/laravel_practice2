<?php
class User
{
    public $conn;
    public function __construct()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        try {
            session_start();
            $this->conn = new PDO("mysql:host=$servername;dbname=crud2", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // echo "Connected successfully";
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function store($data)
    {
        try {
            $name = $data['name'];
            $phone = $data['phone'];
            $query = " insert into user(name, phone) values(:name, :phone)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'name' => $name,
                    'phone' => $phone
                ]
            );
            $_SESSION['success'] = "created!";
            header('location: ../view/index.php');
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/create.php');
        }
    }

    public function retrive_data()
    {
        try {
            $query = "select * from user where is_deleted is null";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/create.php');
        }
    }
    public function show($id)
    {
        try {
            $query = "select * from user where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'id' => $id
                ]
            );
            return $stmt->fetch();
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/index.php');
        }
    }

    public function update($data)
    {
        try {
            $id = $data['id'];
            $name = $data['name'];
            $phone = $data['phone'];
            $query = "update user set name=:name,phone=:phone where id=:id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'id' => $id,
                    'name' => $name,
                    'phone' => $phone
                ]
            );
            $_SESSION['success'] = "Updated!";
            header('location: ../view/index.php');
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/edit.php');
        }
    }
    public function destroy($id)
    {
        try {
            $query = "update user set is_deleted=:value where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [   'value'=>1,
                    'id' => $id
                ]
            );
            $stmt->fetch();
            header('location: ../view/index.php');
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/index.php');
        }
    }
    
    
    public function trash()
    {
        try {
            $query = "select * from user where is_deleted is not null";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/create.php');
        }
    }

    public function restore($id)
    {
        try {

            $query = "update user set is_deleted=:value where id=:id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'id' => $id,
                    'value'=> null
                   
                ]
            );
            $_SESSION['success'] = "restored!";
            header('location: ../view/trash.php');
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/trash.php');
        }
    }



public function delete($id)
    {
        try {
            $query = "delete from user where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(
                [
                    'id' => $id
                ]
            );
            $stmt->fetch();
            header('location: ../view/trash.php');
        } catch (PDOException $e) {
            $_SESSION['error'] = $e->getMessage();
            header('location:  ../view/trash.php');
        }
    }










}
